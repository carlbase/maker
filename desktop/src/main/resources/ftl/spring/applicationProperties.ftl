# datasource
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.url = jdbc:mysql://${dbConnectionData.ipProperty}:${dbConnectionData.portProperty?c}/${dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true
spring.datasource.username = ${dbConnectionData.userNameProperty}
spring.datasource.password = ${dbConnectionData.passwordProperty}
spring.datasource.driverClassName = com.mysql.cj.jdbc.Driver
spring.datasource.maxActive = 25
spring.datasource.minIdle = 3
spring.datasource.initialSize = 10
spring.datasource.connectionTimeout = 30000
spring.datasource.maxWait = 30000
spring.datasource.minEvictableIdleTimeMillis = 30000

# 自动生成代码工具
javafx.title = \u81ea\u52a8\u751f\u6210\u4ee3\u7801\u5de5\u5177
javafx.stage.width = 1000
javafx.stage.height = 600