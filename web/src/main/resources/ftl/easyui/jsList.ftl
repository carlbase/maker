${'$'}(document).ready(function () {
	${'$'}('#info').dialog('close');  // close a window
	
	<#list table.columnList as column>
	<#if column.foreign>
	$("#${column.property}Dialog").dialog("close");
	</#if>
	</#list>
	
	<#list columns as column>
	<#if util.isDate(column)>
	$('#${column.property}').datebox({
		onSelect: function(date){
			<#if column.property?substring(0, 3) == 'end'>
			${util.firstLower(column.property?substring(3))}Check('${column.property}');
			<#elseif column.property?substring(0, 5) == 'start'>
			${util.firstLower(column.property?substring(5))}Check('${column.property}');
			</#if>
		}
	});
	</#if>
	</#list>
	loadPage();
});

var __url__ = _${'$'}ROOT + "${util.firstLower(table.javaName)}/list/json";
function loadPage() {
	${'$'}.ajax({
		type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
		url: __url__
	}).done(function(data) {
		pagination(data);
		
		var p = ${'$'}('#table').datagrid('getPager');  
	    ${'$'}(p).pagination({  
	        pageSize: 10,//每页显示的记录条数，默认为10  
	        pageList: [5,10,20,50,100],//可以设置每页记录条数的列表  
	        beforePageText: '第',//页数文本框前显示的汉字  
	        afterPageText: '页    共 {pages} 页',  
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录',  
	        onSelectPage:function(pageNumber, pageSize){ 
	            loadGridData(pageNumber, pageSize);
	        }
	    });
	});
}

function loadGridData(pageNumber, pageSize) {
	data = "pageNo=" + pageNumber + "&pageSize=" + pageSize + "&" + ${'$'}("#searchForm").serialize();
	${'$'}.ajax({
		type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: __url__ + "?" + data
	}).done(function(data) {
		pagination(data);
	});
}

function pagination(data) {
	var tableData = {total: data.data.total, rows: data.data.beans};
	${'$'}('#table').datagrid("loadData", tableData);
	
}

function search() {
	associationCleans();
	var t = ${'$'}("#searchForm").serialize();
	${'$'}.ajax({
		type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: __url__ + "?" + t
	}).done(function(data) {
		pagination(data);
	});
}

/**********************************数据生成， 修改， 删除******************************************************/
function info_add() {
	${'$'}("#info").dialog("open");
	info_clean();
}

function info_edit() {
	var row = $('#table').datagrid("getSelected");
	if (row == null) {
		$.messager.alert('错误','请选择一行');
		return;
	}
	$('#info').dialog('open').dialog('center').dialog("open");

	$('#${util.firstLower(table.javaName)}Info').form('load',row);
	var tt = false;
	<#list table.columnList as column>
		<#if column.foreign>
			<#list column.foreignKey.foreignTable.columnList as cc>
				<#if cc.property == 'name' || util.contains(cc.property, "Name")>
     $("#${column.property}IdAdd").textbox('setValue', row.${column.property}.${cc.property});
     tt = true;
				</#if>
			</#list>
	if (!tt) {
		$("#${column.property}${util.firstUpper(column.foreignKey.foreignColumn.property)}Add").textbox('setValue',row.${column.property}.${column.foreignKey.foreignColumn.property});
	}
	$("#${column.property}${util.firstUpper(column.foreignKey.foreignColumn.property)}AddHidden").val(row.${column.property}.${column.foreignKey.foreignColumn.property});
    tt = false;
		<#elseif util.isEnum(column)>
    if(row.${column.property}) {
        $('#${column.property}').combobox('setValue', row.${column.property}.value);
    }
		</#if>

	</#list>
}

function info_remove() {
	var item = $('#table').datagrid("getSelected");
	if (item == null) {
		$.messager.alert('错误','请选择一行');
		return;
	}
	$.ajax({
		type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: _${'$'}ROOT + "${util.firstLower(table.javaName)}/deleteById/json/" + item.${table.primaryKey.column.property}
	}).done(function(data) {
		if (data.isSuccess) {
			loadPage();
		}
	});
}

function info_clean() {
	$('#${util.firstLower(table.javaName)}Info').form('clear');
}

function info_click() {
	<#if table.primaryKey.column??>
	var id = $("#${table.primaryKey.column.property}").val();
	if (id) {
		formUpdate();
	} else {
		formAdd();
	}
	</#if>
}

function formAdd() {
	var t = $("#${util.firstLower(table.javaName)}Info").serialize();
	${'$'}.ajax({
		type: "post",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: _$ROOT + "${util.firstLower(table.javaName)}/save/json",
        data: t
	}).done(function(data) {
		if (data.isSuccess) {
			loadPage();
			$("#info").dialog("close");
		}
	});
}

function formUpdate() {
	var t = $("#${util.firstLower(table.javaName)}Info").serialize();
	$.ajax({
		type: "post",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: _${'$'}ROOT + "${util.firstLower(table.javaName)}/save/json",
        data: t
	}).done(function(data) {
		if (data.isSuccess) {
			loadPage();
			$("#info").dialog("close");
		}
	});
}

/**********************************弹出框，现在id*****************************************/
var openMark = null;
<#list table.columnList as column>
<#if column.foreign>
function select${util.firstUpper(column.property)}(mark) {
	openMark = mark;
	$.ajax({
		type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: _${'$'}ROOT + "${util.firstLower(column.foreignKey.foreignTable.javaName)}/list/json"
	}).done(function(data) {
		var tableData = {total: data.data.total, rows: data.data.beans};
		$('#${column.property}Table').datagrid("loadData", tableData);
		
		var p = $('#${column.property}Table').datagrid('getPager');  
	    $(p).pagination({  
	        pageSize: 10,//每页显示的记录条数，默认为10  
	        pageList: [5,10,20,50,100],//可以设置每页记录条数的列表  
	        beforePageText: '第',//页数文本框前显示的汉字  
	        afterPageText: '页    共 {pages} 页',  
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录',  
	        onSelectPage:function(pageNumber, pageSize){ 
	            load${util.firstUpper(column.property)}GridData(pageNumber, pageSize);
	        }
	    });
	});
    
     $("#${column.property}Dialog").dialog("open"); 
}

function load${util.firstUpper(column.property)}GridData(pageNumber, pageSize) {
	data = "pageNo=" + pageNumber + "&pageSize=" + pageSize;
	$.ajax({
		type: "get",//使用get方法访问后台
        dataType: "json",//返回json格式的数据
        url: _${'$'}ROOT + "${util.firstLower(column.foreignKey.foreignTable.javaName)}/list/json?" + data
	}).done(function(data) {
		var tableData = {total: data.data.total, rows: data.data.beans};
		$('#${column.property}Table').datagrid("loadData", tableData);
	});
}

function add${util.firstUpper(column.property)}() {
	var item = $('#${column.property}Table').datagrid("getSelected");
	if (item == null) {
		$.messager.alert('错误','请选择一行');
		return;
	}
	if (openMark === "Add") {
		var tt = false;
		<#list column.foreignKey.foreignTable.columnList as cc>
		<#if cc.property == 'name' || util.contains(cc.property, "Name")>
		$("#${column.property}Id" + openMark).textbox('setValue',(item.${cc.property}));
		tt = true;
		</#if>
		</#list>
		if (!tt) {
			$("#${column.property}Id" + openMark).textbox('setValue',item.${column.foreignKey.foreignTable.primaryKey.column.property});
		}
		$("#${column.property}Id" + openMark + "Hidden").val(item.${column.foreignKey.foreignTable.primaryKey.column.property});
	} else {
		var tt = false;
		<#list column.foreignKey.foreignTable.columnList as cc>
		<#if cc.property == 'name' || util.contains(cc.property, "Name")>
        $("#${column.property}Id" + openMark).val(item.${cc.property});
        tt = true;
		</#if>
		</#list>
		if (!tt) {
			$("#${column.property}Id" + openMark).val(item.${column.foreignKey.foreignTable.primaryKey.column.property});
		}
		$("#${column.property}Id" + openMark + "Hidden").val(item.${column.foreignKey.foreignTable.primaryKey.column.property});
	}
	$("#${column.property}Dialog").dialog("close"); 
}
</#if>
</#list>

/****************************清除关联ID*************************************/
function associationCleans() {
	<#list table.columnList as column>
	<#if column.foreign>
	associationClean('${column.property}Id');
	</#if>
	</#list>
}

function associationClean(id) {
	$("#" + id + "Add").val("");
    $("#" + id + "AddHidden").val("");
}

/***********************时间验证*************************************/
<#list table.columnList as column>
<#if util.isDate(column)>
function ${column.property}Check(id) {
	var startValue = $('#start${util.firstUpper(column.property)}').datetimebox('getValue');
	var endValue = $('#end${util.firstUpper(column.property)}').datetimebox('getValue');
	var startDate = new Date(startValue);
	var endDate = new Date(endValue);
	if (startDate.getTime() > endDate.getTime()) {
		var dd = "#" + id;
		$(dd).datebox('setValue', '');
		$.messager.alert('错误','开始时间不能大于结束时间');
	}
}

</#if>
</#list>