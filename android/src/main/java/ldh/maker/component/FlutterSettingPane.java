package ldh.maker.component;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class FlutterSettingPane extends SettingPane {

    private FlutterTableUi freemarkerTableUi;

    public FlutterSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);

        buildTableUiTab();
    }

    private void buildTableUiTab() {
        freemarkerTableUi = new FlutterTableUi(treeItem, dbName);
        Tab tab = createTab("Table设置", freemarkerTableUi);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                freemarkerTableUi.show();
            }
        });
    }
}
