package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.freemaker.SimpleJavaMaker;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ldh123 on 2018/9/10.
 */
public class FlutterCreateCode extends CreateCode{

    protected String javafxPath = "";

    public FlutterCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    @Override
    public void create() {
        if (isCreate) {
            createOnce();
            isCreate = false;
        }
    }

    protected void createOnce() {
        createMain();

    }

    private void createMain() {
        String lib = createDartPath("lib");
        SimpleJavaMaker simpleJavaMaker = new SimpleJavaMaker();
        simpleJavaMaker.fileName("main.dart")
                .ftl("/ftl/flutter/main.ftl")
                .outPath(lib)
                .data("title", "Flutter自动生成工具")
                .make();

        new SimpleJavaMaker()
                .fileName("home-page.ftl")
                .ftl("/ftl/flutter/home-page.ftl")
                .outPath(createDartPath("lib", "page"))
                .make();

        new SimpleJavaMaker()
                .fileName("welcome-page.ftl")
                .ftl("/ftl/flutter/welcome-page.ftl")
                .outPath(createDartPath("lib", "page"))
                .make();

        new SimpleJavaMaker()
                .fileName("window-util.ftl")
                .ftl("/ftl/flutter/window-util.ftl")
                .outPath(createDartPath("lib", "util"))
                .make();
    }

    protected String createDartPath(String... dirs) {
        String path = FileUtil.getSourceRoot();
        List<String> dirst = new ArrayList<>(Arrays.asList("code", root, getProjectName()));
        Arrays.stream(dirs).forEach(e->dirst.add(e));
        path = makePath(path, dirst);
        return path;
    }
}
