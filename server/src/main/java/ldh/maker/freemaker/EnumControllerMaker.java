package ldh.maker.freemaker;

import ldh.maker.util.FreeMakerUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ldh on 2017/4/23.
 */
public class EnumControllerMaker extends BeanMaker<EnumControllerMaker>  {

    protected List<EnumStatusMaker> enumStatusMakerList = new ArrayList<>();

    public EnumControllerMaker add(EnumStatusMaker enumStatusMaker) {
        enumStatusMakerList.add(enumStatusMaker);
        imports(enumStatusMaker);
        return this;
    }

    @Override
    public EnumControllerMaker make() {
        data();
        out("enumController.ftl", data);
        return this;
    }

    public void data() {
        className = FreeMakerUtil.firstUpper("CommonController");
        fileName = className + ".java";
//        check();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author",author );
        data.put("DATE",str);
        data.put("Description", description);
        data.put("enumStatusMakerList", enumStatusMakerList);
        data.put("className", className);
        super.data();
    }
}
